import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import data from './data.json';

const headings = ['When', 'Ago', 'Description'];

ReactDOM.render(
    <App data={data} title='Open Library API' headings={headings}/>, 
    document.getElementById('root')
    );