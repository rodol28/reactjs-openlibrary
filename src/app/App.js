import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Heading from './components/Heading';
import Row from './components/Row';

import { format } from 'timeago.js';

// ################# HEADINGS
class Headings extends Component {
    render() {
        return <thead className="table-success">
                    <tr>
                        {
                            this.props.headings.map((heading, i) => {
                                return <Heading key={i} heading={heading}/>
                            })
                        }
                    </tr>
                </thead>
    }
}
Headings.propTypes = {    
    headings: PropTypes.array.isRequired
};

// ################# ROW BODY
class Body extends Component {
    render() {
        return <tbody>
                    {
                        this.props.data.map(row => {
                            return <Row key={row.id} row={row}/>
                        })
                    }
                </tbody>
    }
}
Body.propTypes = {
    data: PropTypes.array.isRequired
}

// ################# TABLE
class App extends Component {

    constructor() {
        super();
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        setInterval(async () => {
            const res = await fetch('https://openlibrary.org/recentchanges.json?limit=10&bots=false');
            const data = await res.json();
            const formatData = this.formatData(data);
            this.setState({data: formatData});
        }, 2000);
    }

    formatData(data) {
        return data.map((item) => {
            return {
                'id': item.id,
                'when': format(item.timestamp),
                'who': item.author.key,
                'description': item.comment
            }
        })
    }

    render() {
        return (
                <div className="container p-4">
                    <h1>{this.props.title}</h1>
                    <table className="table table-bordered">
                        <Headings headings={this.props.headings}/>
                        <Body data={this.state.data}/>
                    </table>  
                </div>
                );
    }
}
App.propTypes = {    
    title: PropTypes.string.isRequired,
    headings: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired
};

export default App;