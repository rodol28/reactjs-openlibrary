import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Heading extends Component {
    render() {
        return <th 
                    className="text-center"
                    scope="col">{this.props.heading}
                    </th>
    }
}

Heading.propTypes = {
    heading: PropTypes.string.isRequired
}

export default Heading;