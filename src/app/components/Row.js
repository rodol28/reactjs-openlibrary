import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Row extends Component {
    render() {
        return <tr>
                    <td>{this.props.row.when}</td>
                    <td>{this.props.row.who}</td>
                    <td>{this.props.row.description}</td>
                </tr>
    }
}

Row.propTypes = {
    row: PropTypes.object.isRequired
};

export default Row;